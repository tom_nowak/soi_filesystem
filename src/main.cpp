#include "CommandParser.h"
#include "Filesystem.h"
#include <stdexcept>

int main(int argc, const char **argv)
{
    command_parser::CommandData cmd;
    command_parser::parse(cmd, argc, argv);
    try
    {
        switch(cmd.command)
        {
            case command_parser::INFO:
                global::Filesystem::getInstance().info(cmd.filename);
                break;
            case command_parser::CREATE:
                global::Filesystem::getInstance().create(cmd.filename, cmd.arg1.number, cmd.arg2.number, cmd.arg3.number);
                break;
            case command_parser::DELETE:
                global::Filesystem::getInstance().deleteAll(cmd.filename);
                break;
            case command_parser::RM:
                global::Filesystem::getInstance().rm(cmd.filename, cmd.arg1.name);
                break;
            case command_parser::CPIN:
                global::Filesystem::getInstance().cpin(cmd.filename, cmd.arg1.name, cmd.arg2.name);
                break;
            case command_parser::CPOUT:
                global::Filesystem::getInstance().cpout(cmd.filename, cmd.arg1.name, cmd.arg2.name);
        }
    }
    catch(const std::bad_alloc&) // this CAN happen, because the user sets block size and other parameters
    {
        printf("Critical error - std::bad_alloc! The block size might be too big - cannot create buffers.\n");
    }
    catch(const std::exception &ex) // this should not happen
    {
        printf("Critiacal error - unknown std::exception caught!\n%s\n", ex.what());
    }
    catch(const char *e) // this CAN happen
    {
        printf(e);
    }

    catch(...) // this must not happen in any case - I do not throw other exceptions
    {
        printf("Critical error - unknown exception caught!\n");
    }

    return 0;
}
