#include "Metadata.h"
#include "DebugInline.h"
#include "FilesystemHelpers.h"

using namespace filesystem_helpers;
using namespace global;

Metadata &Metadata::getInstance()
{
    static Metadata instance;
    return instance;
}

void Metadata::init(uint32_t block_size, uint32_t blocks_number, uint32_t hash_table_size)
{
    blockSize = block_size;
    blocksNumber = blocks_number;
    hashTableSize = hash_table_size;
    firstFreeBitmapIndex = 0;
    SILENT_ASSERT(sizeof(FileDescriptor) == 128)
    if((block_size % 128) != 0 || block_size < 256)
        fatalError("Invalid input - block size must be number divisible by 128, at least 256.\n");
    if(hash_table_size == 0)
        fatalError("Invalid input - hash table size must be at least 1.\n");
    if(hash_table_size*3 >= blocks_number)
        fatalError("Invalid input - number of blocks must be more than three times the size of hash table.\n");
    uint32_t bitmap_blocks_number = calculateBitmapBlocksNumber(block_size, blocks_number, hash_table_size);
    bitmapBitsSize = blocks_number - 1 - hash_table_size - bitmap_blocks_number;
    hashTableOffset = 1 + bitmap_blocks_number;
    dataOffset = hashTableOffset + hash_table_size;
    SILENT_ASSERT(dataOffset + bitmapBitsSize == blocks_number)
}

void Metadata::init(const char *filename)
{
    SmartFile file(filename, "rb");
    if(fread(this, sizeof(Metadata), 1, file.getPtr()) != 1)
    {
        fprintf(stderr, "Error reading metadata from file %s - file might be corrupted.\n",
            filename);
        exit(-1);
    }
    if(isContradictory())
    {
        fprintf(stderr, "Error, metadata from file %s is contradictory - file might be corrupted.\n",
            filename);
        exit(-1);
    }
}

bool Metadata::isContradictory() const
{
    if( ((blockSize % 128) != 0 || blockSize < 256) ||
        (hashTableSize == 0) ||
        (hashTableSize*3 >= blocksNumber))
    {
        return 1;
    }
    uint32_t bitmap_blocks_number = calculateBitmapBlocksNumber(blockSize, blocksNumber, hashTableSize);
    if( (bitmapBitsSize != blocksNumber - 1 - hashTableSize - bitmap_blocks_number) ||
        (hashTableOffset != 1 + bitmap_blocks_number) ||
        (dataOffset != hashTableOffset + hashTableSize) ||
        (dataOffset + bitmapBitsSize != blocksNumber))
    {
        return 1;
    }
    return 0;
}

void Metadata::print(FILE *output) const
{
    fprintf(output, "\n"
        " - blockSize: %u\n"
        " - blocksNumber: %u\n"
        " - bitmapBitsSize: %u\n"
        " - hashTableOffset: %u\n"
        " - dataOffset: %u\n",
        blockSize, blocksNumber, bitmapBitsSize, hashTableOffset, dataOffset);
}

uint32_t Metadata::calculateBitmapBlocksNumber(uint32_t block_size, uint32_t blocks_number, uint32_t hash_table_size)
{
    // number of free blocks - for bitmap and data mapped by bitmap:
    // (subtract 1 for block with metadata and hash_table_size for hash table)
    uint32_t free_blocks = blocks_number - 1 - hash_table_size;
    uint32_t free_bytes_in_block0 = block_size - sizeof(Metadata);
    if(free_blocks <= free_bytes_in_block0) // block 0 is enough to map everything
        return 0;
    // Calculating size of bitmap is not trival, as the more free space for data we have, the more
    // blocks for bitmap we need - there is a dependency, so a simple equation will be solved.
    //
    // balance of total space available (bitmap_blocks_number - blocks for bitmap, not including block0):
    // (1:) bitmap_blocks_number + data_blocks_number = free_blocks
    //
    // 8*free_bytes_in_block0 are already mapped by part of bitmap in block0
    // all data blocks need to be mapped:
    // (2:) bitmap_blocks_number*8*block_size >= data_blocks_number - 8*free_bytes_in_block0
    //
    // from (1:)
    // data_blocks_number = free_blocks - bitmap_blocks_number
    //
    // substitute data_blocks_number in (2:)
    // bitmap_blocks_number*8*block_size >= free_blocks - bitmap_blocks_number - 8*free_bytes_in_block0
    // bitmap_blocks_number*(8*block_size + 1) >= free_blocks - 8*free_bytes_in_block0
    // as we want to use as little blocks for bitmap as possible:
    // bitmap_blocks_number = ROUND_UP[ (free_blocks - 8*free_bytes_in_block0) / (8*block_size + 1) ]

    uint32_t bitmap_blocks_number = myDivide(free_blocks - 8*free_bytes_in_block0, 8*block_size + 1);
    SILENT_ASSERT(free_bytes_in_block0*8 + bitmap_blocks_number*8*block_size >= free_blocks - bitmap_blocks_number)
    return bitmap_blocks_number;
}

Metadata::Metadata()
{
#ifdef DEBUG
    printf("unit tests Metadata:\n");
    init(256, 30, 4); // small bitmap - only block 0
    VERBOSE_ASSERT(blockSize == 256)
    VERBOSE_ASSERT(blocksNumber == 30)
    VERBOSE_ASSERT(hashTableSize == 4)
    VERBOSE_ASSERT(bitmapBitsSize = 25)
    VERBOSE_ASSERT(hashTableOffset = 1)
    VERBOSE_ASSERT(dataOffset == 5)
    init(512, 4196, 100); // big bitmap - blocks 0 and 1
    VERBOSE_ASSERT(bitmapBitsSize == 4094)
    VERBOSE_ASSERT(hashTableOffset == 2)
    VERBOSE_ASSERT(dataOffset == 102)
    printf("---------------------------------\n");
#endif
}
