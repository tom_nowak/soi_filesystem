#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "Filesystem.h"
#include "DriveEmulator.h"
#include "Metadata.h"
#include "FilesystemHelpers.h"
#include "DebugInline.h"

using namespace filesystem_helpers;
using namespace global;

Filesystem& Filesystem::getInstance()
{
    static Filesystem instance;
    return instance;
}

void Filesystem::info(const char *fs_name)
{
    Metadata::getInstance().init(fs_name);
    printf("Filesystem %s info:", fs_name);
    Metadata::getInstance().print(stdout);
}

void Filesystem::create(const char *fs_name, uint32_t block_size, uint32_t blocks_number, uint32_t hash_table_size)
{
#ifdef DEBUG
    printf("\nCreating new filesystem with:\n"
           " - fs_name: %s\n"
           " - block_size: %u\n"
           " - blocks_number: %u\n"
           " - hash_table_size: %u\n\n",
           fs_name, block_size, blocks_number, hash_table_size);
#endif

    Metadata::getInstance().init(block_size, blocks_number, hash_table_size);
    DriveEmulator::getInstance().init(fs_name, block_size, blocks_number);
    Buffers buffer(1);
    memset(buffer[0], 0, Metadata::getInstance().blockSize);
    memcpy(buffer[0], &Metadata::getInstance(), sizeof(Metadata));
    DriveEmulator::getInstance().writeBlock(0, buffer[0]);
    printf("Created new filesystem with:");
    Metadata::getInstance().print(stdout);
}

void Filesystem::deleteAll(const char *fs_name)
{
    if(remove(fs_name) == 0)
        printf("delete %s : ok\n", fs_name);
    else
        printf("failed to delete filesystem %s\n", fs_name);
}

void Filesystem::rm(const char *fs_name, const char *file)
{
    Metadata::getInstance().init(fs_name);
    DriveEmulator::getInstance().init(fs_name);
    Buffers buffers(Metadata::getInstance().hashTableOffset + 2 + 1); // buffers for: bitmap, hash table, file
    Bitmap bmap(buffers);
    HashTable htable(buffers, Metadata::getInstance().hashTableOffset);
    void *file_buffer = buffers[buffers.numberOfBuffers - 1];
    const uint32_t length_per_block = DriveEmulator::getInstance().getBlockSize() - sizeof(uint32_t);
    try
    {
        FileDescriptor *fd = htable.find(file);
        uint32_t block = fd->blockNumber;
        while(block)
        {
            DriveEmulator::getInstance().readBlock(block, file_buffer);
            block = *((uint32_t*)((char*)file_buffer + length_per_block));
            bmap.releaseBlock(block);
        }
        htable.remove(fd);
        printf("rm %s : ok\n", file);
    }
    catch(const char *e)
    {
        htable.setErrorFlag();
        bmap.setErrorFlag();
        fprintf(stderr, e);
    }
}

void Filesystem::cpin(const char *fs_name, const char *file_from, const char *file_to)
{
    Metadata::getInstance().init(fs_name);
    DriveEmulator::getInstance().init(fs_name);
    Buffers buffers(Metadata::getInstance().hashTableOffset + 2 + 1); // buffers for: bitmap, hash table, file
    Bitmap bmap(buffers);
    HashTable htable(buffers, Metadata::getInstance().hashTableOffset);
    SmartFile file(file_from, "rb");
    uint64_t file_size = 0;
    uint32_t first_file_block;
    const uint32_t length_to_read = DriveEmulator::getInstance().getBlockSize() - sizeof(uint32_t);
    void *file_buffer = buffers[buffers.numberOfBuffers - 1];
    try
    {
        size_t read_bytes = fread(file_buffer, 1, length_to_read, file.getPtr());
        file_size += read_bytes;
        first_file_block = bmap.acquireFirstDataBlock();
        uint32_t last_block = first_file_block;
        while(read_bytes == length_to_read)
        {
            uint32_t next_block = bmap.acquireFirstDataBlock();
            *((uint32_t*)((char*)file_buffer + length_to_read)) = next_block;
            DriveEmulator::getInstance().writeBlock(last_block, file_buffer);
            last_block = next_block;
            read_bytes = fread(file_buffer, 1, length_to_read, file.getPtr());
            file_size += read_bytes;
        }
        *((uint32_t*)((char*)file_buffer + length_to_read)) = 0; // end of block list
        DriveEmulator::getInstance().writeBlock(last_block, file_buffer);
        FileDescriptor *fd = htable.insert(file_to, bmap);
        fd->fileSize = file_size;
        fd->blockNumber = first_file_block;
        printf("cpin %s %s : ok\n", file_from, file_to);
    }
    catch(const char *e)
    {
        bmap.setErrorFlag();
        htable.setErrorFlag();
        fprintf(stderr, e);
    }
}

void Filesystem::cpout(const char *fs_name, const char *file_to, const char *file_from)
{
    Metadata::getInstance().init(fs_name);
    DriveEmulator::getInstance().init(fs_name);
    Buffers buffers(2 + 1); // buffers for: hash table, file
    HashTable htable(buffers, 0);
    SmartFile file(file_to, "wb");
    void *file_buffer = buffers[buffers.numberOfBuffers - 1];
    const uint32_t length_per_block = DriveEmulator::getInstance().getBlockSize() - sizeof(uint32_t);
    try
    {
        FileDescriptor *fd = htable.find(file_from);
        uint64_t remaining_size = fd->fileSize;
        uint32_t block = fd->blockNumber;
        for(;;)
        {
            DriveEmulator::getInstance().readBlock(block, file_buffer);
            if(length_per_block < remaining_size)
            {
                fwrite(file_buffer, 1, length_per_block, file.getPtr());
                remaining_size -= length_per_block;
            }
            else
            {
                fwrite(file_buffer, 1, remaining_size, file.getPtr());
                break;
            }
            block = *((uint32_t*)((char*)file_buffer + length_per_block));
        }
        printf("cpout %s %s : ok\n", file_to, file_from);
    }
    catch(const char *e)
    {
        htable.setErrorFlag();
        fprintf(stderr, e);
    }
}

Filesystem::Filesystem()
{
#ifdef DEBUG
    Metadata::getInstance().init(256, 10000, 3);
    printf("unit tests Bitmap addressing\n");
    VERBOSE_ASSERT(Bitmap::getBlockBitAddress(0) == Bitmap::BlockBitAddress(0, 0 + 8*sizeof(Metadata)))
    VERBOSE_ASSERT(Bitmap::getBlockBitAddress(9) == Bitmap::BlockBitAddress(0, 9 + 8*sizeof(Metadata)))
    VERBOSE_ASSERT(Bitmap::getAbsoluteAddress(Bitmap::getBlockBitAddress(9)) == 9)
    VERBOSE_ASSERT(Bitmap::getAbsoluteAddress(Bitmap::getBlockBitAddress(334)) == 334)
    VERBOSE_ASSERT(Bitmap::getAbsoluteAddress(Bitmap::getBlockBitAddress(2017)) == 2017)
    VERBOSE_ASSERT(Bitmap::getAbsoluteAddress(Bitmap::getBlockBitAddress(4000)) == 4000)
    VERBOSE_ASSERT(Bitmap::getBlockBitAddress(Bitmap::getAbsoluteAddress(Bitmap::BlockBitAddress(2, 17)))
                   == Bitmap::BlockBitAddress(2, 17))
    printf("---------------------------------\n");

//  The following tests have been run in previous Metadata/BitmapBlock implementation.
//  All tests have been passed and tested functions have not been changed.
//  Unfortunately, I cannot run there tests in current implementation.

//    printf("---------------------------------\n"
//           "Running simple unit tests.\n"
//           "---------------------------------\n");
//    {
//        printf("test BitmapBlock:\n");
//        uint8_t buffer[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 4, 0, 0, 0, 0, 64};
//        BitmapBlock bmap(buffer, 16*8);
//        VERBOSE_ASSERT(bmap.findBit(1))
//        VERBOSE_ASSERT(bmap.getLastFoundPosition() == 8*8)
//        VERBOSE_ASSERT(bmap.findBit(1))
//        VERBOSE_ASSERT(bmap.getLastFoundPosition() == 10*8 + 2)
//        VERBOSE_ASSERT(bmap.findBit(1))
//        VERBOSE_ASSERT(bmap.getLastFoundPosition() == 16*8 - 2)
//        VERBOSE_ASSERT(!bmap.findBit(1))
//        VERBOSE_ASSERT(bmap.getLastFoundPosition() == bmap.getTotalSize())
//        bmap.resetActPosition();
//        VERBOSE_ASSERT(bmap.findBit(0))
//        VERBOSE_ASSERT(bmap.getLastFoundPosition() == 0)
//        bmap.toggleBit(7);
//        VERBOSE_ASSERT(bmap.findBit(1))
//        VERBOSE_ASSERT(bmap.getLastFoundPosition() == 7)
//        printf("---------------------------------\n");
//    }
//    {
//        printf("test FileDescriptorBlockMetadata:\n");
//        Metadata::getInstance().init(256, 10, 2);
//        Buffers buff(1, Metadata::getInstance().blockSize);
//        std::memset(buff[0], 0, Metadata::getInstance().blockSize);
//        FileDescriptorBlockMetadata *fdm = reinterpret_cast<FileDescriptorBlockMetadata*>(buff[0]);
//        VERBOSE_ASSERT(fdm->find("abc") == -1)
//        fdm->numberOfValidDescriptors = 1;
//        FileDescriptor *fd = reinterpret_cast<FileDescriptor*>(fdm+1);
//        fd->fileSize = 10;
//        strcpy(fd->name, "abc");
//        VERBOSE_ASSERT(fdm->find("abc") == 1)
//        VERBOSE_ASSERT(fdm->find("ab") == 0)
//        printf("---------------------------------\n");
//    }
#endif
}

Filesystem::~Filesystem()
{}
