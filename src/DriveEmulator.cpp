#include "DriveEmulator.h"
#include <error.h>
#include <cstdio>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstdlib>
#include <cstring>

#include "DebugInline.h"

using namespace global;

DriveEmulator& DriveEmulator::getInstance()
{
    static DriveEmulator instance;
    return instance;
}

void DriveEmulator::init(const char *filename)
{
    SILENT_ASSERT(("DriveEmulator::init1", data == nullptr && fd < 0)) // not initialized yet
    // open: filename, read and write, read and write for all
    fd = open(filename, O_RDWR, 0666);
    if(fd < 0)
        fatalPerror("Error when opening virtual drive\nerrno");
    if(read(fd, &blockSize, 4) < 4)
        fatalPerror("Failed to read block size from file\nerrno");
    if(read(fd, &blocksNumber, 4) < 4)
        fatalPerror("Failed to read blocks number from file\nerrno");
    lseek(fd, 0, SEEK_SET); // go back to beginning of the file
    size_t file_size = (size_t)blockSize * (size_t)blocksNumber;
    // map file to memory, pages with read | write permission, MAP_SHARED - propagate changes to file
    data = (char*)mmap(0, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(data == (void*)-1)
        fatalPerror("Failed to map file into memory (filesystem metadata can be corrupted)\nerrno");
    SILENT_ASSERT(("DriveEmulator::init1", memcmp(&blockSize, data, 4) == 0))
    SILENT_ASSERT(("DriveEmulator::init1", memcmp(&blocksNumber, data + 4, 4) == 0))
}

void DriveEmulator::init(const char *filename, uint32_t block_size, uint32_t blocks_number)
{
    SILENT_ASSERT(("DriveEmulator::init2", data == nullptr && fd < 0)) // not initialized yet
    blockSize = block_size;
    blocksNumber = blocks_number;
    // open: filename, read and write | create if does not exit | fail is exists, read and write for all
    fd = open(filename, O_RDWR | O_CREAT | O_EXCL, 0666);
    if(fd < 0)
        fatalPerror("Error when creating virtual drive\nerrno");
    size_t file_size = (size_t)blockSize * (size_t)blocksNumber;
    if(ftruncate(fd, file_size) < 0)
        fatalPerror("Failed to truncate file after creation\nerrno");
    // map file to memory, pages with read | write permission, MAP_SHARED - propagate changes to file
    data = (char*)mmap(0, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if(data == (void*)-1)
        fatalPerror("Failed to map file into memory (after creating it)\nerrno");
    memcpy(data, &blockSize, 4);
    memcpy(data + 4, &blocksNumber, 4);
}

void DriveEmulator::writeBlock(uint32_t number, const void *buffer)
{
    SILENT_ASSERT(("DriveEmulator::writeBlock", number < blocksNumber))
    size_t offset = (size_t)number * (size_t)blockSize;
    memcpy(data + offset, buffer, blockSize);
    ++writesNumber;
}

DriveEmulator::DriveEmulator()
    : data(nullptr), blockSize(0), blocksNumber(0), fd(-1), readsNumber(0), writesNumber(0)
{}

DriveEmulator::~DriveEmulator()
{
    if(data)
        munmap(data, (size_t)blockSize * (size_t)blocksNumber);
    if(fd > 0)
        close(fd);
    printf("# DriveEmulator statistics - reads: %llu, writes: %llu.\n",
        readsNumber, writesNumber);
}

void DriveEmulator::readBlock(uint32_t number, void *buffer)
{
    SILENT_ASSERT(("DriveEmulator::readBlock", number < blocksNumber))
    size_t offset = (size_t)number * (size_t)blockSize;
    memcpy(buffer, data + offset, blockSize);
    ++readsNumber;
}
