#include "CommandParser.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>

namespace
{
    void printHelp(const char *program_name)
    {
        printf("Usage: %s\n"
               " <path to virtual drive> <command> [<command arguments>]\n"
               "Available commands <arguments> (comment): \n"
               "  info (prints information about filesystem)\n"
               "  create <size of one block> <number of blocks> <size of hash table with file descriptors>\n"
               "  delete (deletes the whole filesystem)\n"
               "  rm <filename on virtual drive> (removes file from filesystem)\n"
               "  cpin <path in main OS> <filename on virtual drive> (copies from main OS to virtual filesystem)\n"
               "  cpout <path in main OS> <filename on virtual drive> (copies from virtual filesystem to main OS)\n",
               program_name );
        exit(1);
    }

    void printSmallHelp(const char *command, const char *args)
    {
        printf("Command %s error, expected:\n"
               "  <path to virtual drive> %s %s\n",
               command, command, args);
        exit(1);
    }
}

void command_parser::parse(CommandData &retval, int argc, const char **argv)
{
    if(argc < 3)
        printHelp(argv[0]);
    retval.filename = argv[1];
    if(strcmp(argv[2], "info") == 0)
    {
        retval.command = INFO;
        if(argc != 3)
            printSmallHelp("info", "");
    }
    else if(strcmp(argv[2], "create") == 0)
    {
        retval.command = CREATE;
        if(argc != 6)
            printSmallHelp("create", "<size of one block> <number of blocks> <size of hash table with file descriptors>");
        retval.arg1.number = atoi(argv[3]);
        retval.arg2.number = atoi(argv[4]);
        retval.arg3.number = atoi(argv[5]);
    }
    else if(strcmp(argv[2], "delete") == 0)
    {
        retval.command = DELETE;
        if(argc != 3)
            printSmallHelp("delete", "");
    }
    else if(strcmp(argv[2], "rm") == 0)
    {
        retval.command = RM;
        if(argc != 4)
            printSmallHelp("rm", "<filename on virtual drive> ");
        retval.arg1.name = argv[3];
    }
    else if(strcmp(argv[2], "cpin") == 0)
    {
        retval.command = CPIN;
        if(argc != 5)
            printSmallHelp("cpin", "<path in main OS> <filename on virtual drive>");
        retval.arg1.name = argv[3];
        retval.arg2.name = argv[4];
    }
    else if(strcmp(argv[2], "cpout") == 0)
    {       
        retval.command = CPOUT;
        if(argc != 5)
            printSmallHelp("cpout", "<path in main OS> <filename on virtual drive>");
        retval.arg1.name = argv[3];
        retval.arg2.name = argv[4];
    }
    else
        printHelp(argv[0]);
}
