#include "FilesystemHelpers.h"
#include "DriveEmulator.h"
#include "Metadata.h"
#include <limits>
#include <functional>
#include <cstring>

using namespace filesystem_helpers;
using namespace global;

//
// SmartFile
//

SmartFile::SmartFile(const char *filename, const char *mode)
{
    ptr = fopen(filename, mode);
    if(!ptr)
    {
        fprintf(stderr, "Error - could not open file \"%s\" in mode \"%s\".\n",
                filename, mode);
        std::exit(-1);
    }
}

SmartFile::~SmartFile()
{
    if(ptr)
        fclose(ptr);
}

FILE *SmartFile::getPtr()
{
    return ptr;
}


//
// FileDescriptorBlockMetadata
//

int32_t FileDescriptorBlockMetadata::find(const char *filename)
{
    const uint32_t fdsNumber = Metadata::getInstance().blockSize / sizeof(FileDescriptor);
    uint32_t first_free_place = fdsNumber;
    for(uint32_t i = 1; i < fdsNumber; ++i) // for each file descriptor in block
    {
        FileDescriptor *fd = reinterpret_cast<FileDescriptor*>(this + i);
        if(fd->fileSize != 0) // valid file descriptor
        {
            if(strncmp(filename, fd->name, FileDescriptor::NAME_LENGTH) == 0)
                return static_cast<int32_t>(i); // found
        }
        else // unused file descriptor
        {
            if(i < first_free_place)
                first_free_place = i;
        }
    }
    if(first_free_place == fdsNumber)
        return 0; // no free place
    return -static_cast<int32_t>(first_free_place); // free place at returned (-index)
}


//
// Buffers
//

Buffers::Buffers(unsigned number_of_buffers)
    : numberOfBuffers(number_of_buffers)
{
    SILENT_ASSERT(Metadata::getInstance().blockSize % sizeof(FileDescriptor) == 0)
    data = new char[number_of_buffers * Metadata::getInstance().blockSize];
}

Buffers::~Buffers()
{
    delete [] data;
}

void *Buffers::operator[](unsigned i)
{
    SILENT_ASSERT(i < numberOfBuffers)
    return data + i * Metadata::getInstance().blockSize;
}


//
// BitmapBlock
//

uint32_t BitmapBlock::totalSize;
uint32_t BitmapBlock::registerEndIndex;

// actPosition is initialized to (positon_begin - 1) (0u - 1u = <uint32_t>::max())
// to simplify usage of findBit() - see below
BitmapBlock::BitmapBlock(void *buffer, uint32_t positon_begin)
    : bufferBegin(buffer), lastFoundPosition(positon_begin - 1)
{
    totalSize = Metadata::getInstance().blockSize * 8;
    registerEndIndex = myDivide(totalSize, BITS_IN_REGISTER);
}

bool BitmapBlock::findBit(bool value)
{
    // On one hand, after successful findBit() call actPosition points to the found bit.
    // On the otther hand, I want successful findBit() calls to find new values - so actPosition
    // is incremented in each findBit() call. To make first findBit() call correct, I initialized
    // actPosiotion to std::numeric_limits<uint32_t>::max() - OVERFLOW ON PURPOSE.
    ++lastFoundPosition;
    SILENT_ASSERT(lastFoundPosition < totalSize)
    register_t register_buffer;
    // anti_pattern - if value in register_buffer==anti_pattern, then there is no point in checking each bit
    const register_t anti_pattern = value ? 0 : std::numeric_limits<register_t>::max();
    register_t *ptr = reinterpret_cast<register_t*>(bufferBegin);
    uint32_t register_index = lastFoundPosition/BITS_IN_REGISTER;
    uint32_t bit_index_in_register = lastFoundPosition % BITS_IN_REGISTER;
    for(; register_index < registerEndIndex; ++register_index)
    {
        register_buffer = ptr[register_index];
        if(register_buffer != anti_pattern)
        {
            do
            {
                if((bool) ( (1l << bit_index_in_register) & register_buffer ) == value)
                {
                    lastFoundPosition = register_index*BITS_IN_REGISTER + bit_index_in_register;
                    if(lastFoundPosition >= totalSize)
                    {
                        lastFoundPosition = totalSize;
                        return 0;
                    }
                    return 1;
                }
            } while(++bit_index_in_register < BITS_IN_REGISTER);
        }
        bit_index_in_register = 0;
    }
    lastFoundPosition = totalSize;
    return 0;
}

bool BitmapBlock::getBit(uint32_t index)
{
    SILENT_ASSERT(index < totalSize)
    uint32_t byte_index = index/8;
    uint32_t index_in_byte = index%8;
    uint8_t mask = 1 << index_in_byte;
    return ((uint8_t*)bufferBegin)[byte_index] & mask;
}

void BitmapBlock::setBit(uint32_t index, bool value)
{
    SILENT_ASSERT(index < totalSize)
    uint32_t byte_index = index/8;
    uint32_t index_in_byte = index%8;
    uint8_t mask = 1 << index_in_byte;
    if(value)
        ((uint8_t*)bufferBegin)[byte_index] |= mask;
    else
        ((uint8_t*)bufferBegin)[byte_index] &= (~mask);
}

void BitmapBlock::toggleBit(uint32_t index)
{
    SILENT_ASSERT(index < totalSize)
    uint32_t byte_index = index/8;
    uint32_t index_in_byte = index%8;
    uint8_t mask = 1 << index_in_byte;
    ((uint8_t*)bufferBegin)[byte_index] ^= mask;
}

void *BitmapBlock::getBufferBegin()
{
    return bufferBegin;
}

uint32_t BitmapBlock::getLastFoundPosition()
{
    return lastFoundPosition;
}

void BitmapBlock::setLastFoundPosition(uint32_t val)
{
    SILENT_ASSERT(val < totalSize)
    lastFoundPosition = val;
}

void BitmapBlock::resetActPosition()
{
    lastFoundPosition = std::numeric_limits<uint32_t>::max();
}

uint32_t BitmapBlock::getTotalSize()
{
    return totalSize;
}


//
// Bitmap
//

Bitmap::Bitmap(Buffers &b)
    : errorFlag(0), buffers(b)
{
    DriveEmulator::getInstance().readBlock(0, buffers[0]);
    map.emplace(0, AccessedBlock(buffers[0], sizeof(Metadata))); // sizeof(Metadata) - offset in block 0
}

Bitmap::~Bitmap()
{
    if(errorFlag)
        return;
    for(auto it = map.begin(); it != map.end(); ++it)
    {
        AccessedBlock &ab = it->second;
        if(ab.isModified)
            DriveEmulator::getInstance().writeBlock(it->first, ab.block.getBufferBegin());
    }
}

uint32_t Bitmap::getAbsoluteAddress(Bitmap::BlockBitAddress bba)
{
    if(bba.blockNumber == 0)
        return bba.bitInBlock - 8*sizeof(Metadata);
    uint32_t bits_in_block0 = (Metadata::getInstance().blockSize - sizeof(Metadata)) * 8;
    return bits_in_block0 + 8 * Metadata::getInstance().blockSize * (bba.blockNumber - 1) + bba.bitInBlock;
}

Bitmap::BlockBitAddress Bitmap::getBlockBitAddress(uint32_t absolute_address)
{
    const uint32_t bits_in_block0 = (Metadata::getInstance().blockSize - sizeof(Metadata)) * 8;
    const uint32_t bits_in_block = Metadata::getInstance().blockSize * 8;
    if(absolute_address < bits_in_block0)
        return BlockBitAddress(0, absolute_address + 8*sizeof(Metadata));
    absolute_address -= bits_in_block0;
    return BlockBitAddress(absolute_address / bits_in_block + 1,
                           absolute_address % bits_in_block);
}

bool Bitmap::get(Bitmap::BlockBitAddress bba)
{
    AccessedBlock &ab = accessBlock(bba);
    return ab.block.getBit(bba.bitInBlock);
}

void Bitmap::releaseBlock(BlockBitAddress bba)
{
    AccessedBlock &ab = accessBlock(bba);
    ab.isModified = 1;
    ab.block.setBit(bba.bitInBlock, 0);
    uint32_t index = getAbsoluteAddress(bba);
    SILENT_ASSERT(index != Metadata::getInstance().firstFreeBitmapIndex)
    if(index < Metadata::getInstance().firstFreeBitmapIndex)
    {
        map.find(0)->second.setModified(); // modify metadata
        Metadata::getInstance().firstFreeBitmapIndex = index;
    }
}

void Bitmap::releaseBlock(uint32_t index)
{
    SILENT_ASSERT(index != Metadata::getInstance().firstFreeBitmapIndex)
    if(index < Metadata::getInstance().firstFreeBitmapIndex)
    {
        map.find(0)->second.setModified(); // modify metadata
        Metadata::getInstance().firstFreeBitmapIndex = index;
    }
    BlockBitAddress bba = getBlockBitAddress(index);
    AccessedBlock &ab = accessBlock(bba);
    ab.isModified = 1;
    ab.block.setBit(bba.bitInBlock, 0);
}

uint32_t Bitmap::acquireFirstDataBlock()
{
    const uint32_t first_free = Metadata::getInstance().firstFreeBitmapIndex;
    if(first_free == Metadata::getInstance().bitmapBitsSize)
        fatalError("Error - there is no free space in virtual drive.\n");
    map.find(0)->second.setModified(); // metadata block will be modified
    BlockBitAddress bba = getBlockBitAddress(first_free);
    AccessedBlock &ab = accessBlock(bba);
    ab.isModified = 1;
    ab.block.setBit(bba.bitInBlock, 1);
    // find the next free position to make the next call of this function successful:
    ab.block.setLastFoundPosition(bba.bitInBlock);
    if(ab.block.findBit(0))
    {
        bba.bitInBlock = ab.block.getLastFoundPosition();
        Metadata::getInstance().firstFreeBitmapIndex = getAbsoluteAddress(bba);
        return first_free + Metadata::getInstance().dataOffset;
    }
    bba.bitInBlock = 0; // to start searching from the beginning of the block
    while(++bba.blockNumber < Metadata::getInstance().hashTableOffset) // for all remaining bitmap blocks
    {
        ab = accessBlock(bba);
        if(ab.block.findBit(0))
        {
            bba.bitInBlock = ab.block.getLastFoundPosition();
            Metadata::getInstance().firstFreeBitmapIndex = getAbsoluteAddress(bba);
            return first_free + Metadata::getInstance().dataOffset;
        }
    }
    // not found - the next call to this function will fail (this was the last succesful):
    Metadata::getInstance().firstFreeBitmapIndex = Metadata::getInstance().bitmapBitsSize;
    VERBOSE_ASSERT(("occupyFirstFreeBlock, end of free space",
                    getAbsoluteAddress(bba) == Metadata::getInstance().bitmapBitsSize))
    return first_free + Metadata::getInstance().dataOffset;
}

Bitmap::AccessedBlock& Bitmap::accessBlock(Bitmap::BlockBitAddress bba)
{
    SILENT_ASSERT(bba.blockNumber < Metadata::getInstance().hashTableOffset)
    auto retval = map.insert(std::pair<uint32_t, AccessedBlock>
                             (bba.blockNumber, AccessedBlock(buffers[bba.blockNumber], bba.bitInBlock)));
    SILENT_ASSERT(retval.first->first == bba.blockNumber)
    SILENT_ASSERT(retval.first->second.block.getBufferBegin() == buffers[bba.blockNumber])
    if(retval.second) // block has been inserted
        DriveEmulator::getInstance().readBlock(bba.blockNumber, buffers[bba.blockNumber]);
    return retval.first->second;
}


//
// HashTable
//

HashTable::HashTable(Buffers &b, uint32_t buffer_index)
    : buffers(b), bufferIndex(buffer_index), isChanged0(0), isChanged1(0), errorFlag(0)
{}

HashTable::~HashTable()
{
    if(errorFlag)
        return;
    if(isChanged0)
    {
        VERBOSE_ASSERT(blockNumber0 >= Metadata::getInstance().hashTableOffset)
        DriveEmulator::getInstance().writeBlock(blockNumber0, buffers[bufferIndex]);
    }
    if(isChanged1)
    {
        VERBOSE_ASSERT(blockNumber1 >= Metadata::getInstance().hashTableOffset)
        DriveEmulator::getInstance().writeBlock(blockNumber1, buffers[bufferIndex+1]);
    }
}

FileDescriptor *HashTable::find(const char *filename)
{
    FileDescriptorBlockMetadata *fdbm = reinterpret_cast<FileDescriptorBlockMetadata*>(buffers[bufferIndex]);
    blockNumber0 = getHashIndex(filename);
    do
    {
        DriveEmulator::getInstance().readBlock(blockNumber0, fdbm);
        int64_t retval = fdbm->find(filename);
        if(retval > 0)
            return reinterpret_cast<FileDescriptor*>(fdbm + retval);
    } while((blockNumber0 = fdbm->nextBlock) != 0);
    fatalError("Error: file to find does not exist in virtual filesystem.\n");
    return nullptr; // only to suppress compiler warning
}

FileDescriptor *HashTable::insert(const char *filename, Bitmap &bmap)
{
    FileDescriptor *fd = findPlaceForNewDescriptor(filename);
    if(fd)
    {
        strncpy(fd->name, filename, FileDescriptor::NAME_LENGTH);
        FileDescriptorBlockMetadata *fdbm = reinterpret_cast<FileDescriptorBlockMetadata*>(buffers[bufferIndex]);
        ++fdbm->numberOfValidDescriptors;
        isChanged0 = 1;
        return fd;
    }
    blockNumber1 = bmap.acquireFirstDataBlock();
    DriveEmulator::getInstance().readBlock(Metadata::getInstance().dataOffset + blockNumber1,
                                           buffers[bufferIndex+1]);
    FileDescriptorBlockMetadata *fdbm = reinterpret_cast<FileDescriptorBlockMetadata*>(buffers[bufferIndex]);
    fdbm->nextBlock = blockNumber1;
    fdbm = reinterpret_cast<FileDescriptorBlockMetadata*>(buffers[bufferIndex+1]);
    fdbm->nextBlock = 0;
    fdbm->numberOfValidDescriptors = 1;
    fd = reinterpret_cast<FileDescriptor*>(fdbm+1);
    strncpy(fd->name, filename, FileDescriptor::NAME_LENGTH);
    isChanged0 = 1;
    isChanged1 = 1;
    return fd;
}

void HashTable::remove(FileDescriptor *fd)
{
    // Simple, inefficient implementation, it can be changed later (empty blocks should be deleted).
    FileDescriptorBlockMetadata *fdbm = reinterpret_cast<FileDescriptorBlockMetadata*>(buffers[bufferIndex]);
    fd->fileSize = 0; // mark fd as invalid/unused
    --fdbm->numberOfValidDescriptors;
    isChanged0 = 1;
}

uint32_t HashTable::getHashIndex(const char *filename)
{
    static std::hash<std::string> hasher;
#ifdef DEBUG
    printf("hasher(%s) = %lu\n", filename, hasher(filename));
#endif
    return static_cast<uint32_t>
            (hasher(filename) % static_cast<size_t>(Metadata::getInstance().hashTableSize))
            + Metadata::getInstance().hashTableOffset;
}

FileDescriptor *HashTable::findPlaceForNewDescriptor(const char *filename)
{
    FileDescriptorBlockMetadata *fdbm = reinterpret_cast<FileDescriptorBlockMetadata*>(buffers[bufferIndex]);
    blockNumber0 = getHashIndex(filename);
    do
    {
        DriveEmulator::getInstance().readBlock(blockNumber0, fdbm);
        int64_t retval = fdbm->find(filename);
        if(retval > 0)
            fatalError("Error: file to insert already exists in virtual filesystem.\n");
        else if(retval < 0)
        {
            FileDescriptor *found_place = reinterpret_cast<FileDescriptor*>(fdbm - retval);
            blockNumber1 = fdbm->nextBlock;
            if(blockNumber1 == 0)
                return found_place;
            fdbm = reinterpret_cast<FileDescriptorBlockMetadata*>(buffers[bufferIndex+1]);
            do
            {
                DriveEmulator::getInstance().readBlock(blockNumber1, fdbm);
                if(fdbm->find(filename) > 0)
                    fatalError("Error: file to insert already exists in virtual filesystem.\n");
            } while((blockNumber1 = fdbm->nextBlock) != 0);
            return found_place;
        }
    } while((blockNumber0 = fdbm->nextBlock) != 0);
    return nullptr;
}
