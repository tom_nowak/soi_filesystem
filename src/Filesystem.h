#pragma once
#include <cstdint>
#include <cstdio>

// Filesystem structure:
// 1. constant metadata (see struct Metadata in FilesystemHelpers.h)
// 2. bitmap of used blocks, starting in block0
// 3. hash table of file descriptors (see struct FileDescriptor in FilesystemHelpers.h)
// 4. blocks of data, which can be used to store files or file descriptors (in case of hash collisions)

namespace global
{
    class Filesystem
    {
    public:
        static Filesystem& getInstance(); // singleton
        void info(const char *fs_name);
        void create(const char *fs_name, uint32_t block_size, uint32_t blocks_number, uint32_t hash_table_size);
        void deleteAll(const char *fs_name);
        void rm(const char *fs_name, const char *file);
        void cpin(const char *fs_name, const char *file_from, const char *file_to);
        void cpout(const char *fs_name, const char *file_to, const char *file_from);

    private:
        Filesystem();
        ~Filesystem();
    };
}
