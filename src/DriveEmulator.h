#pragma once
#include <cstdint>

namespace global
{
    class DriveEmulator
    {
    public:
        static DriveEmulator& getInstance(); //singleton

        // to read virtual drive from file:
        void init(const char *filename);
        // to create new virtual drive:
        void init(const char *filename, uint32_t block_size, uint32_t blocks_number);
        uint32_t getBlockSize() { return blockSize; }
        uint32_t getBlocksNumber() { return blocksNumber; }
        void readBlock(uint32_t number, void *buffer);
        void writeBlock(uint32_t number, const void *buffer);

    private:
        DriveEmulator();
        ~DriveEmulator();

        char *data;
        uint32_t blockSize;
        uint32_t blocksNumber;
        int fd; // low-level file descriptor
        unsigned long long readsNumber, writesNumber;
    };
}
