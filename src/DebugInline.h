#pragma once
#include <cstdlib>
#include <cstdio>

inline void fatalPerror(const char *msg)
{
    perror(msg);
    exit(-1);
}

inline void fatalError(const char *msg)
{
    throw(msg);
}

#ifdef DEBUG
#define VERBOSE_ASSERT(x) if((x)) printf("Assert " #x " ok\n"); else fatalError("Assert " #x " failed!\n");
#else
#define VERBOSE_ASSERT(x)
#endif

#ifdef DEBUG
#define SILENT_ASSERT(x) if(!(x)) fatalError("Assert " #x " failed!\n");
#else
#define SILENT_ASSERT(x)
#endif
