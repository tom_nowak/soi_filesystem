#pragma once
#include <cstdint>
#include <cstdio>
#include <map>
#include "DebugInline.h"

namespace filesystem_helpers
{
    // forward declarations:
    class SmartFile;
    struct FileDescriptor;
    struct FileDescriptorBlockMetadata;
    class Buffers;
    class BitmapBlock;
    class HashTable;

    // Divide two numbers (T should be an integer type) and round UP the reult.
    template<typename T>
    T myDivide(T a, T b)
    {
        T retval = a/b;
        if(a % b)
            return retval + 1;
        return retval;
    }

    class SmartFile
    {
    public:
        SmartFile(const char *filename, const char *mode);
        ~SmartFile();
        FILE* getPtr();

    private:
        FILE *ptr;
    };

    // each file is described by 1 file descriptor, file descriptors will be in blocks in the filesystem
    struct FileDescriptor
    {
        static constexpr int NAME_LENGTH = 128 - sizeof(uint32_t) - sizeof(uint64_t);
        uint64_t fileSize; // in bytes
        uint32_t blockNumber;
        char name[NAME_LENGTH];
    };

    struct FileDescriptorBlockMetadata
    {
        uint32_t numberOfValidDescriptors;
        uint32_t nextBlock;
        // this struct must have the same size as FileDescriptor:
        char padding[sizeof(FileDescriptor) - 2*sizeof(uint32_t)];

        // returns:
        // if found: index of found file descriptor
        // if not found and there is free space in block: (-index) of first unused FileDescripor
        // if not found and there is no free space: 0
        int32_t find(const char *filename);
    };

    // class representing set of buffers
    // all buffers are allocated at once for performance reasons
    class Buffers
    {
    public:
        const unsigned numberOfBuffers;

        Buffers(unsigned number_of_buffers);
        ~Buffers();
        Buffers(Buffers&) = delete;
        Buffers& operator=(Buffers&) = delete;
        void* operator[](unsigned i);

    private:
        char *data;
    };

    // class representing one bitmap block
    // it operates on a buffer, but does not allocate/deallocate it
    class BitmapBlock
    {
    public:
        static constexpr uint32_t BITS_IN_REGISTER = sizeof(register_t)*8;

        BitmapBlock(void *buffer, uint32_t position_begin = 0);
        bool findBit(bool value); // starting after lastFoundPosition
        bool getBit(uint32_t index); // index of bit
        void setBit(uint32_t index, bool value);
        void toggleBit(uint32_t index);

        void *getBufferBegin();
        uint32_t getLastFoundPosition();
        void setLastFoundPosition(uint32_t val);
        void resetActPosition();
        uint32_t getTotalSize();

    private:
        void *bufferBegin;
        uint32_t lastFoundPosition; // in bits, 0-based index

        static uint32_t totalSize; // in bits
        static uint32_t registerEndIndex;
    };

    class Bitmap
    {
    public:
        struct BlockBitAddress
        {
            uint32_t blockNumber;
            uint32_t bitInBlock;

            BlockBitAddress(uint32_t blockN, uint32_t bitN)
                : blockNumber(blockN), bitInBlock(bitN)
            {}

            bool operator==(const BlockBitAddress &other) // to eanble unit testing
            {
                return blockNumber == other.blockNumber && bitInBlock == other.bitInBlock;
            }
        };

        Bitmap(Buffers &b);
        ~Bitmap(); // saves modified blocks (unless errorFlag is set, see below)

        // functions to simplify bit addressing:
        static uint32_t getAbsoluteAddress(BlockBitAddress bba);
        static BlockBitAddress getBlockBitAddress(uint32_t absolute_address);

        bool get(BlockBitAddress bba);
        bool get(uint32_t index) { return get(getBlockBitAddress(index)); }
        void releaseBlock(BlockBitAddress bba);
        void releaseBlock(uint32_t index);
        uint32_t acquireFirstDataBlock();
        void setErrorFlag() { errorFlag = 1; } // no changes will be "commited" to the drive

    private:
        struct AccessedBlock
        {
            bool isModified;
            BitmapBlock block;

            AccessedBlock(void *buffer, uint32_t position_begin = 0)
                : isModified(0), block(buffer, position_begin)
            {}

            void setModified() { isModified = 1; }
        };

        // if errorFlag is set, absolutely no changes will be "commited" to the drive
        bool errorFlag;

        // Buffers [0, metadata.hashTableOffset) will be used - the whole bitmap.
        // This class does NOT allocate or deallocate buffers.
        Buffers &buffers;

        // This map saves all blocks that have been accessed so that there is no need to
        // read them from the drive multiple times. Moreover, struct AccessedBlock keeps track of
        // changes, so that Bitmap destructor knows which blocks to save ("commit" changes to drive).
        std::map<uint32_t, AccessedBlock> map;

        AccessedBlock& accessBlock(BlockBitAddress bba);
    };

    class HashTable
    {
    public:
        HashTable(Buffers &b, uint32_t buffer_index);
        ~HashTable();
        FileDescriptor *find(const char *filename);
        FileDescriptor *insert(const char *filename, Bitmap &bmap);
        void remove(FileDescriptor *fd); // fd MUST be found with find()
        void setErrorFlag() { errorFlag = 1; } // no changes will be "committed" to the drive
        static uint32_t getHashIndex(const char *filename);

    private:
        Buffers &buffers;
        const uint32_t bufferIndex; // buffers number: bufferIndex and bufferIndex+1 will be used
        uint32_t blockNumber0, blockNumber1;
        bool isChanged0, isChanged1; // if buffer has been changed and needs to be "commited"
        bool errorFlag; // if set, no changes will be saved to drive

        FileDescriptor* findPlaceForNewDescriptor(const char *filename);
    };
}
