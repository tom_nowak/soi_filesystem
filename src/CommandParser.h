#pragma once
#include <cstdint>

namespace command_parser
{
    enum CommandType { INFO, CREATE, DELETE, RM, CPIN, CPOUT };

    struct CommandData
    {
        CommandType command;
        const char *filename;
        union Arg
        {
            uint32_t number;
            const char *name;
        } arg1, arg2, arg3;
    };

    void parse(CommandData &retval, int argc, const char **argv);
}
