#pragma once
#include <cstdint>
#include <cstdio>

namespace global
{
    // structure saved at the beginning of the filesystem, contains, as the name says metadata
    class Metadata
    {
    public:
        // given by the user as parameters:
        uint32_t blockSize;
        uint32_t blocksNumber;
        uint32_t hashTableSize; // in blocks
        // auxiliary - calculated based on the parameters:
        uint32_t bitmapBitsSize; // equals number of data blocks
        uint32_t hashTableOffset; // in blocks
        uint32_t dataOffset; // in blocks
        uint32_t firstFreeBitmapIndex;
        uint32_t padding; // unused now (it may be changed later), needed to make sizeof(Metadata) divisible by sizeof(uin64_t)

        static Metadata& getInstance(); // singleton
        void init(uint32_t block_size, uint32_t blocks_number, uint32_t hash_table_size);
        void init(const char *filename);
        bool isContradictory() const;
        void print(FILE *output) const;
        static uint32_t calculateBitmapBlocksNumber(uint32_t block_size, uint32_t blocks_number, uint32_t hash_table_size);

    private:
        Metadata();
        ~Metadata() {}
    };

}
